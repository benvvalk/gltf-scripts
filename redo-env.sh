#------------------------------------------------------------
# Shared environment for all .redo scripts.
#------------------------------------------------------------

# Redirect STDOUT to STDERR for all commands.
#
# NOTE: `redo` allows either writing to STDOUT
# or writing to $3 to produce the build output
# for a .redo script. If a .redo script 
# writes to both STDOUT and $3, `redo` will die
# with an error.
exec >&2

die()
{
    echo "$@" >&2
    exit 1
}

export TMPDIR=/mnt/d/tmp
