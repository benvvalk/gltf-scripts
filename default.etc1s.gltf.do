. ./redo-env.sh

basename="$(basename "$2")"
dirname="$(dirname "$1")"

gltf="$basename/$basename.gltf"
redo-ifchange "$gltf"

# Note: Running `gltf-transform copy` on
# a etc1s-compressed .glb file is not an option
# here, because `gltf-transform copy` does not
# preserve draco/ktx compression.  In other words,
# the output of `gltf-transform copy` is always
# a vanilla .gltf/.glb file without draco/ktx
# compression.

tmpdir="$(mktemp --tmpdir -d etc1s-gltf.XXXXXX)"

cp "$basename"/* "$tmpdir"

gltf-transform etc1s "$tmpdir/$basename.gltf" "$tmpdir/$basename.etc1s.gltf"

mkdir -p "$dirname"

rsync -av \
      --exclude="$basename.gltf" \
      --exclude="$basename.etc1s.gltf" \
      --exclude='*.png' \
      --exclude='*.jpg' \
      "$tmpdir"/ "$dirname"

mv "$tmpdir/$basename.etc1s.gltf" "$3"

# if output file already exists, treat it as up to date
echo exists | redo-stamp
