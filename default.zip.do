# load code/environment shared by all .redo scripts
. ./redo-env.sh

basename="$(basename "$2")"
gltf="$basename/$basename.gltf"

redo-ifchange "$basename/$basename.gltf"

# Note: --junk-paths strips the leading directory paths
# (i.e. $basename) from the files when adding them to the
# archive. We could temporarily `cd` to "$tmpdir" to
# achieve the same result, but this is more concise.
zip --junk-paths "$3" "$basename"/*

# if .zip exists, treat it as up to date
echo exists | redo-stamp
