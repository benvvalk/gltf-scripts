. ./redo-env.sh

basename="$(basename "$1" .etc1s.glb)"
glb="$basename.glb"

#test -f "$glb" || die "$0: error: '$glb' not found"

redo-ifchange "$glb"

# We must first create the .etc1s.glb file in
# temporary directory for a combination
# of two reasons:
#
# (1) `gltf-transform copy` dies with a cryptic
# ENOENT error unless the second filename argument
# has a .gltf/.glb extension.
#
# (2) `redo` requires us to write the final output
# to `$3`, which is a temporary file that does not
# have a .gltf/.glb extension.

tmpdir="$(mktemp --tmpdir -d etc1s-glb.XXXXXX)"

gltf-transform etc1s "$glb" "$tmpdir/$basename.etc1s.glb"

mv "$tmpdir/$basename.etc1s.glb" "$3"

# if output file already exists, treat it as up to date
echo exists | redo-stamp
