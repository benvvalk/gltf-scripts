. ./redo-env.sh

basename="$(basename "$1" .draco.glb)"
glb="$basename.glb"

#test -f "$glb" || die "$0: error: '$glb' not found"

redo-ifchange "$glb"

# We must first create the .draco.glb file in
# temporary directory for a combination
# of two reasons:
#
# (1) `gltf-transform copy` dies with a cryptic
# ENOENT error unless the second filename argument
# has a .gltf/.glb extension.
#
# (2) `redo` requires us to write the final output
# to `$3`, which is a temporary file that does not
# have a .gltf/.glb extension.

tmpdir="$(mktemp --tmpdir -d draco-glb.XXXXXX)"

gltf-transform draco "$glb" "$tmpdir/$basename.draco.glb"

mv "$tmpdir/$basename.draco.glb" "$3"

# if output file already exists, treat it as up to date
echo exists | redo-stamp
