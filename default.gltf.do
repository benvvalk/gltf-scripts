# load code/environment shared by all .redo scripts
. ./redo-env.sh

# search for .glb with same name in current directory or ancestor directory
basename="$(basename "$2")"
dirname="$(dirname "$2")"

glb="$basename.glb"

redo-ifchange "$glb"

# Convert to .glb to .gltf in a temporary directory.
# 
# Note that redo will die with an error if we try writeto
# to the output .gltf file directly. Instead we must
# write to the temporary file provided in `$3`, to
# ensure that script is atomic.

tmpdir="$(mktemp --tmpdir -d glb-to-gltf.XXXXXX)"

gltf-transform copy "$glb" "$tmpdir/$basename.gltf"

mkdir -p "$dirname"
rsync -av --exclude="$basename.gltf" "$tmpdir"/ "$dirname"
mv "$tmpdir/$basename.gltf" "$3"

# if .gltf exists, treat it as up to date
echo exists | redo-stamp
