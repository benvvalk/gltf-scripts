. ./redo-env.sh

redo-ifchange "$2.zip"

tmpdir="$(mktemp --tmpdir -d zip-to-glb.XXXXXX)"
unzip -d "$tmpdir" "$2.zip"

# Note:`gltf-transform copy` dies with a cryptic
# ENOENT error if the second argument does not
# end with .gltf/.glb
gltf-transform copy "$tmpdir"/*.gltf "$tmpdir/$2.glb"

mv "$tmpdir/$2.glb" "$3"

# if .glb exists, treat it as up to date
echo exists | redo-stamp
